﻿using System;
using Xamarin.Forms;


namespace TransScribe
{
	public class EventView : AbsoluteLayout
	{
		public Label Name { get; set; }
		public Editor Description { get; set; }
		public Button Button { get; set; }

		public EventView ( TSData data, TimelineView timeline )
		{
			double width = timeline.PixelFromDuration (data.EndTime.Subtract (data.StartTime));

			Name = new Label {
				Text = data.Name,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				BackgroundColor = Color.Transparent,
				WidthRequest = (width < 10) ? width : width - 10,
				HeightRequest = 15,
				XAlign = TextAlignment.Center,
			};

			Description = new Editor {
				Text = data.Description,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				BackgroundColor = Color.Transparent,
				WidthRequest = (width < 10) ? width : width - 10,
				HeightRequest = 70,
				IsEnabled = false,
			};

			Button = new Button {
				BorderRadius = 8,
				BackgroundColor = data.Color,
				BorderColor = Color.Gray,
				BorderWidth = 1,
				WidthRequest = width,
				HeightRequest = 95
			};

			Children.Add (Button,new Point(0,0));
			Children.Add (Name,new Point(5,5));
			Children.Add (Description,new Point(5,20));

			//BackgroundColor = Color.Purple;
			WidthRequest = width;
			HeightRequest = 95;
		}			
	}
}

