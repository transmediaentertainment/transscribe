﻿using System;
using Xamarin.Forms;

namespace TransScribe
{
	public class TSData
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public Color Color { get; set; } 
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
		public int[] PreviousId { get; set; }
		public int[] NextId { get; set; }

		public TSData ( int id, string name, string description, Color color, DateTime startTime, DateTime endTime )
		{
			Id = id;
			Name = name;
			Description = description;
			Color = color;
			StartTime = startTime;
			EndTime = endTime;
		}
	}
}

