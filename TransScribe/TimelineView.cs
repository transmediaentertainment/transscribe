﻿using System;
using Xamarin.Forms;
using System.Diagnostics;


namespace TransScribe
{
	public class TimelineView : AbsoluteLayout
	{
		public enum TimeMarker {
			Second,
			Second15,
			Minute,
			Minute15,
			Hour,
			Hour6,
			Day
		}

		public DateTime TimelineStart { get; set; }
		public DateTime TimelineEnd { get; set; }

		public double PixelsPerSecond { get; set; }

		private const double c_MinLabelPixels = 200;
		private const double c_MinMarkPixels = 10;

		public TimelineView ( DateTime timelineStart, DateTime timelineEnd, int widthRequest, int heightRequest)
		{
			TimelineStart = timelineStart;
			TimelineEnd = timelineEnd;
			WidthRequest = widthRequest;
			HeightRequest = heightRequest;
			BackgroundColor = Color.White;

			TimeSpan timeSpan = TimelineEnd.Subtract (TimelineStart);
			string labelText = null;

			PixelsPerSecond = (double)widthRequest / timeSpan.TotalSeconds;

			Debug.WriteLine ("Span:" + timeSpan.ToString () + " SS:" + timeSpan.TotalSeconds + " PPS:" + PixelsPerSecond + " SM:" + timeSpan.TotalMinutes + " SH:" + timeSpan.TotalHours );
			Debug.WriteLine ("PPM:" + PixelsPerSecond * 60 + " PPH:" + PixelsPerSecond * 60 * 60);

			double timelineSecondCounter = 0;
			double addSeconds = c_MinMarkPixels;

			// find first marker
			TimeMarker currentMarkerType = TimeMarker.Second;
			timelineSecondCounter = RoundUpSeconds (TimelineStart, timelineSecondCounter, out addSeconds, out currentMarkerType, out labelText);

			do {
				BoxView marker = new BoxView {
					Color = Color.Black,
				};
				switch ( currentMarkerType ) {
					case TimeMarker.Day:
						marker.HeightRequest = 35;
						marker.WidthRequest = 4;
						break;
					case TimeMarker.Hour6:
						marker.HeightRequest = 30;
						marker.WidthRequest = 3;
						break;
					case TimeMarker.Hour:
						marker.HeightRequest = 25;
						marker.WidthRequest = 3;
						break;
					case TimeMarker.Minute15:
						marker.HeightRequest = 20;
						marker.WidthRequest = 2;
						break;
					case TimeMarker.Minute:
						marker.HeightRequest = 15;
						marker.WidthRequest = 2;
						break;
					case TimeMarker.Second15:
						marker.HeightRequest = 10;
						marker.WidthRequest = 1;
						break;
					case TimeMarker.Second:
						marker.HeightRequest = 5;
						marker.WidthRequest = 1;
						break;
				}

				Children.Add(marker,new Point( timelineSecondCounter * PixelsPerSecond, 20 ) );

				if ( labelText != null ) {
					Label label = new Label {
						Text = labelText,
						TextColor = Color.Black,
						XAlign = TextAlignment.Start,
					};
					Children.Add(label,new Point( timelineSecondCounter * PixelsPerSecond, 1));
				}

				timelineSecondCounter += addSeconds;
				timelineSecondCounter = RoundUpSeconds( TimelineStart, timelineSecondCounter, out addSeconds, out currentMarkerType, out labelText );

			} while (timelineSecondCounter < timeSpan.TotalSeconds);

		}

		public double PixelFromDateTime( DateTime dateTime ) {
			return dateTime.Subtract (TimelineStart).TotalSeconds * PixelsPerSecond;
		}

		public double PixelFromDuration( TimeSpan timeSpan ) {
			return timeSpan.TotalSeconds * PixelsPerSecond;
		}

		private double RoundUpSeconds( DateTime startTime, double currentSeconds, out double addSeconds, out TimeMarker timeMarker, out string labelText ) {

			// default to no label
			labelText = null;

			DateTime currentDateTime = startTime.AddSeconds (currentSeconds);

			// determine minimum marker seconds
			if (PixelsPerSecond > c_MinMarkPixels) {
				// seconds is the min marker
				addSeconds = 1;
			} else if (PixelsPerSecond * 15 > c_MinMarkPixels) {
				// 15 seconds
				if (currentDateTime.Second == 0 || currentDateTime.Second == 15 || currentDateTime.Second == 30 || currentDateTime.Second == 45) {
					// this position is good to start
				} else if (currentDateTime.Second < 15) {
					currentSeconds += 15 - currentDateTime.Second;
				} else if (currentDateTime.Second < 30) {
					currentSeconds += 30 - currentDateTime.Second;
				} else if (currentDateTime.Second < 45) {
					currentSeconds += 45 - currentDateTime.Second;
				} else if (currentDateTime.Second < 60) {
					currentSeconds += 60 - currentDateTime.Second;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}
				addSeconds = 15;
			} else if (PixelsPerSecond * 30 > c_MinMarkPixels) {
				// 30 seconds
				if (currentDateTime.Second == 0 || currentDateTime.Second == 30) {
					// this position is good to start
				} else if (currentDateTime.Second < 30) {
					currentSeconds += 30 - currentDateTime.Second;
				} else if (currentDateTime.Second < 60) {
					currentSeconds += 60 - currentDateTime.Second;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}
				addSeconds = 30;
			} else if (PixelsPerSecond * 60 > c_MinMarkPixels) {
				// 1 minute
				if (currentDateTime.Second == 0) {
					// this position is good to start
				} else if (currentDateTime.Second < 60) {
					currentSeconds += 60 - currentDateTime.Second;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}
				addSeconds = 60;
			} else if (PixelsPerSecond * 60 * 15 > c_MinMarkPixels) { 
				// 15 minutes

				// to nearest minute
				if (currentDateTime.Second == 0) {
					// this position is good to start
				} else if (currentDateTime.Second < 60) {
					currentSeconds += 60 - currentDateTime.Second;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}

				// to nearest 15 minutes
				if (currentDateTime.Minute == 0 || currentDateTime.Minute == 15 || currentDateTime.Minute == 30 || currentDateTime.Minute == 45) {
					// this position is good to start
				} else if (currentDateTime.Minute < 15) {
					currentSeconds += (15 - currentDateTime.Minute) * 60;
				} else if (currentDateTime.Minute < 30) {
					currentSeconds += (30 - currentDateTime.Minute) * 60;
				} else if (currentDateTime.Minute < 45) {
					currentSeconds += (45 - currentDateTime.Minute) * 60;
				} else if (currentDateTime.Minute < 60) {
					currentSeconds += (60 - currentDateTime.Minute) * 60;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}
				addSeconds = 60 * 15;
			} else if (PixelsPerSecond * 60 * 30 > c_MinMarkPixels) {
				// 30 minutes

				// to nearest minute
				if (currentDateTime.Second == 0) {
					// this position is good to start
				} else if (currentDateTime.Second < 60) {
					currentSeconds += 60 - currentDateTime.Second;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}

				// to nearest 30 minutes
				if (currentDateTime.Minute == 0 || currentDateTime.Minute == 30) {
					// this position is good to start
				} else if (currentDateTime.Minute < 30) {
					currentSeconds += (30 - currentDateTime.Minute) * 60;
				} else if (currentDateTime.Minute < 60) {
					currentSeconds += (60 - currentDateTime.Minute) * 60;
				} else {
					Debug.WriteLine ("ERROR - illegal seconds value");
				}
				addSeconds = 60 * 30;
			} else if (PixelsPerSecond * 60 * 60 > c_MinMarkPixels) {
				addSeconds = 60 * 60;
				// TODO: 1 hour
			} else if (PixelsPerSecond * 60 * 60 * 6 > c_MinMarkPixels) {
				addSeconds = 60 * 60 * 6;
				// TODO: 6 hours
			} else if (PixelsPerSecond * 60 * 60 * 12 > c_MinMarkPixels) {
				addSeconds = 60 * 60 * 12;
				// TODO: 12 hours
			} else if (PixelsPerSecond * 60 * 60 * 24 > c_MinMarkPixels) {
				addSeconds = 60 * 60 * 24;
				// TODO: 1 Day +?
			} else {
				addSeconds = 60 * 60 * 24; // ??
			}

			DateTime movedTime = startTime.AddSeconds (currentSeconds);
			//Debug.WriteLine ("RoundUpSeconds:" + currentSeconds + " DT:" + movedTime);

			if (movedTime.Second == 0 && movedTime.Minute == 0 && movedTime.Hour == 0) {
				timeMarker = TimeMarker.Day;
			} else if (movedTime.Second == 0 && movedTime.Minute == 0 && (movedTime.Hour == 6 || movedTime.Hour == 12 || movedTime.Hour == 18)) {
				timeMarker = TimeMarker.Hour6;
			} else if (movedTime.Second == 0 && movedTime.Minute == 0 ) {
				timeMarker = TimeMarker.Hour;
			} else if (movedTime.Second == 0 && (movedTime.Minute == 15 || movedTime.Minute == 30 || movedTime.Minute == 45) ) {
				timeMarker = TimeMarker.Minute15;
			} else if (movedTime.Second == 0) {
				timeMarker = TimeMarker.Minute;
			} else if (movedTime.Second == 15 || movedTime.Second == 30 || movedTime.Second == 45) {
				timeMarker = TimeMarker.Second15;
			} else {
				timeMarker = TimeMarker.Second;
			}

			if (PixelsPerSecond > c_MinMarkPixels) {
				// seconds is the min marker - label on 
				if (movedTime.Second == 0 || movedTime.Second == 15 || movedTime.Second == 30 || movedTime.Second == 45) { 
					labelText = movedTime.ToString("hh:mm:ss tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 15 > c_MinMarkPixels) {
				// 15 seconds
				if (movedTime.Second == 0 && movedTime.Minute % 5 == 0 ) { 
					labelText = movedTime.ToString("hh:mm tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 30 > c_MinMarkPixels) {
				// 30 seconds - label evey 10 minutes
				//if (movedTime.Minute == 0 || movedTime.Minute == 10 || movedTime.Minute == 20 || movedTime.Minute == 30 || movedTime.Minute == 40 || movedTime.Minute == 50 ) { 
				if (movedTime.Second == 0 && (movedTime.Minute == 0 || movedTime.Minute == 15 || movedTime.Minute == 30 || movedTime.Minute == 45)) { 
					labelText = movedTime.ToString("hh:mm tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 60 > c_MinMarkPixels) {
				// 1 minute - label every 15 minutes
				if (movedTime.Second == 0 && (movedTime.Minute == 0 || movedTime.Minute == 15 || movedTime.Minute == 30 || movedTime.Minute == 45)) { 
					labelText = movedTime.ToString("hh:mm tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 60 * 15 > c_MinMarkPixels) { 
				// 15 minutes
				if (movedTime.Second == 0 && movedTime.Minute == 0 && movedTime.Hour == 0 ) { 
					labelText = movedTime.ToString("hh:mm tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 60 * 30 > c_MinMarkPixels) {
				// 30 minutes
				if (movedTime.Second == 0 && movedTime.Minute == 0 && movedTime.Hour % 2 == 0 ) { 
					labelText = movedTime.ToString("hh:mm tt ddd dd-MMM-yy");
				}
			} else if (PixelsPerSecond * 60 * 60 > c_MinMarkPixels) {
				// TODO: 1 hour
			} else if (PixelsPerSecond * 60 * 60 * 6 > c_MinMarkPixels) {
				// TODO: 6 hours
			} else if (PixelsPerSecond * 60 * 60 * 12 > c_MinMarkPixels) {
				// TODO: 12 hours
			} else if (PixelsPerSecond * 60 * 60 * 24 > c_MinMarkPixels) {
				// TODO: 1 Day +?
			}


			return currentSeconds;
		}
	}
}

