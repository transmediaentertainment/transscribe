﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TransScribe
{
	public class App
	{
		public static List<TSData> dataList;

		public App()
		{
		}

		public static Page GetMainPage ()
		{	
			Label headerLabel = new Label {
				Text = "TransScribe",
				XAlign = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				BackgroundColor = Color.FromHex("F0F0F0"),
				TextColor = Color.Black,
				HeightRequest = 20,
			};

			// populate some test data
			dataList = new List<TSData>();
			dataList.Add(new TSData(0,"First Event","This is a description of the first event",Color.Green,new DateTime(2014,10,10,8,0,0),new DateTime(2014,10,10,8,5,0)));
			dataList.Add(new TSData(1,"Second Event","This is a description of the second event",Color.Aqua,new DateTime(2014,10,10,8,0,0),new DateTime(2014,10,10,8,10,0)));
			dataList.Add(new TSData(2,"Third Event","This is a description of the third event",Color.Lime,new DateTime(2014,10,10,8,5,0),new DateTime(2014,10,10,8,10,0)));
			dataList.Add(new TSData(3,"Fourth Event","This is a description of the fourth event",Color.Green,new DateTime(2014,10,10,8,10,0),new DateTime(2014,10,10,8,20,0)));
			dataList.Add(new TSData(4,"Fifth Event","This is a description of the fifth event",Color.Aqua,new DateTime(2014,10,10,8,15,0),new DateTime(2014,10,10,8,30,0)));
			dataList.Add(new TSData(5,"Sixth Event","This is a description of the sixth event",Color.Lime,new DateTime(2014,10,10,8,20,0),new DateTime(2014,10,10,8,21,0)));
			dataList.Add(new TSData(6,"Seventh Event","This is a description of the seventh event",Color.Green,new DateTime(2014,10,10,8,25,0),new DateTime(2014,10,10,8,30,0)));
			dataList.Add(new TSData(7,"Eigth Event","This is a description of the eighth event",Color.Aqua,new DateTime(2014,10,10,8,40,0),new DateTime(2014,10,10,8,45,0)));
			dataList.Add(new TSData(8,"Ninth Event","This is a description of the ninth event",Color.Lime,new DateTime(2014,10,10,8,50,0),new DateTime(2014,10,10,9,0,0)));
			dataList.Add(new TSData(9,"Tenth Event","This is a description of the tenth event",Color.Green,new DateTime(2014,10,10,9,10,0),new DateTime(2014,10,10,9,30,0)));

			int i;
			TimelineView timelineView = new TimelineView ( new DateTime(2014,10,10,7,45,0), new DateTime(2014,10,10,9,45,0), 2000, 2000 );

			for ( i=0; i<dataList.Count; i++ ) {
				double eventXPos = timelineView.PixelFromDateTime (App.dataList [i].StartTime);
				if (eventXPos >= 0 && eventXPos <= 2000) {
					timelineView.Children.Add (new EventView (App.dataList [i],timelineView), new Point (eventXPos, 50 + i * 100));
				}
			}

			ScrollView scrollView = new ScrollView {
				Orientation = ScrollOrientation.Horizontal,
				Content = new ScrollView {
					Content = timelineView,
					IsEnabled = true,
					Orientation = ScrollOrientation.Vertical,
				}
			};

			StackLayout mainPageLayout = new StackLayout {
				Children = {
					headerLabel,
					scrollView
				}
			};
								
			return new ContentPage {
				Content = mainPageLayout
			};	

		}

	}
}

